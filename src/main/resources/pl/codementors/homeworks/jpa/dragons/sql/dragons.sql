CREATE USER dragons WITH PASSWORD 'dragons';

CREATE DATABASE dragons OWNER dragons;

CREATE TABLE dragons (
	id SERIAL PRIMARY KEY,
	name VARCHAR (256),
	color VARCHAR (256),
	wingspan int);

CREATE TABLE eggs (
	id SERIAL PRIMARY KEY,
	weight int,
	diameter int,
	id_dragon int,
	FOREIGN KEY (id_dragon) REFERENCES dragons (id));