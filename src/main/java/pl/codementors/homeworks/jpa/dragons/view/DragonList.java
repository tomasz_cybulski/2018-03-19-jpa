package pl.codementors.homeworks.jpa.dragons.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.homeworks.jpa.dragons.DragonsDAO;
import pl.codementors.homeworks.jpa.dragons.EggsDAO;
import pl.codementors.homeworks.jpa.dragons.model.Dragon;
import pl.codementors.homeworks.jpa.dragons.model.Egg;

import java.net.URL;
import java.util.ResourceBundle;

public class DragonList implements Initializable {

    @FXML
    private TableView<Dragon> dragonsTable;

    @FXML
    private TableColumn<Dragon, Integer> idDragonColumn;

    @FXML
    private TableColumn<Dragon, String> nameColumn;

    @FXML
    private TableColumn<Dragon, String> colorColumn;

    @FXML
    private TableColumn<Dragon, Integer> wingspanColumn;

    @FXML
    private TableView<Egg> eggsTable;

    @FXML
    private TableColumn<Egg, Integer> idEggColumn;

    @FXML
    private TableColumn<Egg, Integer> weightColumn;

    @FXML
    private TableColumn<Egg, Integer> diameterColumn;

    @FXML
    private TableColumn<Egg, Integer> idDragonInEggTableColumn;

    private ObservableList<Dragon> dragons = FXCollections.observableArrayList();

    private ObservableList<Egg> eggs = FXCollections.observableArrayList();

    private DragonsDAO dragonsDAO = new DragonsDAO();

    private EggsDAO eggsDAO = new EggsDAO();

    private ObjectProperty<Dragon> selectedDragon = new SimpleObjectProperty<>(null);


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dragonsTable.setItems(dragons);
        eggsTable.setItems(eggs);

        selectedDragon.bind(dragonsTable.getSelectionModel().selectedItemProperty());
        selectedDragon.addListener(new ChangeListener<Dragon>() {
            @Override
            public void changed(ObservableValue<? extends Dragon> observableValue, Dragon oldValue, Dragon newValue) {
                eggs.clear();
                if (newValue != null) {
                    eggs.addAll(newValue.getEggs());
                }
            }
        });

        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setName(event.getNewValue());
            merge(dragon);
        });

        wingspanColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        wingspanColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setWingspan(event.getNewValue());
            merge(dragon);
        });

        colorColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        colorColumn.setOnEditCommit(event -> {
            Dragon dragon = event.getRowValue();
            dragon.setColor(event.getNewValue());
            merge(dragon);
        });

        dragons.addAll(dragonsDAO.findAll());

    }

    @FXML
    private void addDragon(ActionEvent event) {
        Dragon dragon = new Dragon("grey");
        dragon = dragonsDAO.persist(dragon);
        dragons.add(dragon);

    }

    @FXML
    private void removeDragon(ActionEvent event){
        if (selectedDragon != null) {
            dragonsDAO.delete(selectedDragon.get());
            dragons.remove(selectedDragon.get());
            dragonsTable.getSelectionModel().clearSelection();
        }
    }

    /*@FXML
    private void addEgg(ActionEvent event) {
        Dragon dragon = dragonsTable.getSelectionModel().getSelectedItem();
        if (dragon != null) {
            Egg egg = new Egg();
            egg.setDragon(dragon);
            egg = eggsDAO.persist(egg);
            dragon.getEggs().add(egg);
            eggs.add(egg);
        }
    }

    @FXML
    private void removeEgg(ActionEvent event){
        Egg egg = eggsTable.getSelectionModel().getSelectedItem();
        if (egg != null) {
            egg.getDragon().getEggs().remove(egg);
            eggs.remove(egg);
            merge(egg.getDragon());
            eggsTable.getSelectionModel().clearSelection();
        }
    }*/

    private void merge(Dragon dragon) {
        int index = dragons.indexOf(dragon);
        dragon = dragonsDAO.merge(dragon);
        dragons.set(index, dragon);
        dragonsTable.getSelectionModel().select(index);
    }
}
