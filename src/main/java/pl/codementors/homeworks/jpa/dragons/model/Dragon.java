package pl.codementors.homeworks.jpa.dragons.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "dragons")
public class Dragon {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String color;

    @Column
    private int wingspan;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dragon", fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Egg> eggs = new ArrayList<>();

    public Dragon() {
    }

    public Dragon(String color) {
        this("change name", color, 0);
    }

    public Dragon(String name, String color, int wingspan) {
        this.name = name;
        this.color = color;
        this.wingspan = wingspan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public List<Egg> getEggs() {
        return eggs;
    }

    public void setEggs(List<Egg> eggs) {
        this.eggs = eggs;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", color=" + color +
                ", wingspan=" + wingspan +
                '}';
    }
}
