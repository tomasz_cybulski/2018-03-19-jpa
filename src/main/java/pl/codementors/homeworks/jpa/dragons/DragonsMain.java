package pl.codementors.homeworks.jpa.dragons;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class DragonsMain extends Application {

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL fxml = DragonsMain.class.getResource("/pl/codementors/homeworks/jpa/dragons/view/dragon_list.fxml");
        ResourceBundle rb = ResourceBundle.getBundle("pl.codementors.homeworks.jpa.dragons.view.messages.dragons_list");

        FXMLLoader loader = new FXMLLoader(fxml, rb);
        Parent root = loader.load();

        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("/pl/codementors/homeworks/jpa/dragons/view/css/dragons_list.css");

        stage.setTitle(rb.getString("applicationTitle"));//Set application title.
        stage.setScene(scene);

        stage.setOnCloseRequest(event -> DragonsEntityManagerFactory.close());

        stage.show();
    }
}
