package pl.codementors.homeworks.jpa.dragons;

import pl.codementors.homeworks.jpa.dragons.model.Dragon;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class DragonsDAO {

    public Dragon persist(Dragon dragon){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(dragon);
        entityTransaction.commit();
        entityManager.close();
        return dragon;
    }

    public Dragon merge(Dragon dragon){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        dragon = entityManager.merge(dragon);
        entityTransaction.commit();
        entityManager.close();
        return dragon;
    }

    public void delete(Dragon dragon){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.remove(entityManager.merge(dragon));
        entityTransaction.commit();
        entityManager.close();
    }

    public Dragon find(int id){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        Dragon dragon = entityManager.find(Dragon.class, id);
        entityManager.close();
        return dragon;
    }

    public List<Dragon> findAll(){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        List<Dragon> dragons = entityManager.createQuery("select m from Dragon m").getResultList();
        entityManager.close();
        return dragons;
    }
}
