package pl.codementors.homeworks.jpa.dragons;


import pl.codementors.homeworks.jpa.dragons.model.Egg;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class EggsDAO {

    public Egg persist(Egg egg){
        EntityManager entityManager = DragonsEntityManagerFactory.createEntityManager();
        EntityTransaction entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        entityManager.persist(egg);
        entityTransaction.commit();
        entityManager.close();
        return egg;
    }
}
