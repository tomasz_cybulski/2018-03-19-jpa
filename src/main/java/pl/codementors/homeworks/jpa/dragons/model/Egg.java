package pl.codementors.homeworks.jpa.dragons.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "eggs")
public class Egg {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private int weight;

    @Column
    private int diameter;

    @ManyToOne
    @JoinColumn(name = "id_dragon", referencedColumnName = "id")
    private Dragon dragon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public Dragon getDragon() {
        return dragon;
    }

    public void setDragon(Dragon dragon) {
        this.dragon = dragon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Egg egg = (Egg) o;
        return id == egg.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
